﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worddle
{
    /// <summary>
    /// Clase amb diferents metodes que conformen el menu del programa
    /// </summary>
    class Menu
    {
        InData data = new InData();
        FilesController FilesController = new FilesController();
        LanguageController LanguageController = new LanguageController();
        /// <summary>
        /// Metode que controla el menu 
        /// </summary>
        public void MenuController()
        {
            bool finish;
            do
            {
                data.ShowMenu();
                finish = MenuOptions();
            } while (finish != true);
        }
        /// <summary>
        /// Metode que retorna un boolea. Si es true tancar el programa
        /// </summary>
        /// <returns>Retorna un boolea</returns>
        public bool MenuOptions()
        {
            WorddleController worddle = new WorddleController();
            int answer = data.ReadInt();
            switch (answer)
            {
                case 1:
                    worddle.WorddleStart();
                    break;
                case 2:
                    FilesController.ShowRanking();
                    break;
                case 3:
                    LanguageController.ChangeLanguage();
                    break;
                case 0:
                    return true;
            }
            return false;
        }


    }
}
