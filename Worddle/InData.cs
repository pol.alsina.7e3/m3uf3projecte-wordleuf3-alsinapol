﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace Worddle
{
    /// <summary>
    /// Clase que conte diferents metode de'entrada de dades, sortida i manipulacio
    /// </summary
    class InData
    {
        /// <summary>
        /// Mostrar el menu del worddle
        /// </summary>
        public void ShowMenu()
        {
            Console.WriteLine("»»——————————————　(>'.'<)　———————————««");
            Console.WriteLine("(>'.'<) BENVINGUTS A Worddle (>'.'<)");
            Console.WriteLine("»»——————————————　(>'.'<)　———————————««");
            Console.WriteLine("Aquest joc consisteix en adivinar una paraula de 5 lletres. Tens 6 intents.");
            Console.WriteLine("Quan estiguis preparat Introdueix [1]");
            Console.WriteLine("Si vols visualitzar el rank introdueix [2]");
            Console.WriteLine("Si vols canviar idioma a angles introdueix [3]");
            Console.WriteLine("Sortir [0]");
        }
        /// <summary>
        /// Metode que llegeix un numeric donat per l'usuari
        /// </summary>
        /// <returns>Retorna numeric llegit per l'usuari</returns>
        public int ReadInt()
        {
            return Convert.ToInt32(Console.ReadLine());
        }
        /// <summary>
        /// Metode que agafa les lletres que introdueix l'usuari i comproba que siguin 5 lletres si o si
        /// </summary>
        /// <returns>Retorna les lletres intorduïdes per l'usuari</returns>
        public string ReadStringAnswer()
        {
            string wordPlayerAnswer;
            do
            {
                Console.WriteLine("Introdueix les lletras:");
                wordPlayerAnswer = Console.ReadLine();
            } while (wordPlayerAnswer != null && wordPlayerAnswer.Length != 5);
            return wordPlayerAnswer;
        }
        /// <summary>
        /// Metode que demana el nom de l'usuari i ha de ser de nomes tres lletres
        /// </summary>
        /// <returns>Retorna un string amb el nom de l'usuari</returns>
        public string PlayerName()
        {
            string namePlayer;
            Console.WriteLine("Introdueix el teu nom? (nomes 3 lletres)");
            do
            {
                namePlayer = Console.ReadLine()?.ToUpper();

            } while (namePlayer != null && namePlayer.Length != 3);
            return namePlayer;
        }
        /// <summary>
        /// Posar al la variable el nom del jugador i tambe li pregunta utilitzant el metode
        /// </summary>
        /// <param name="name">nom del jugador</param>
        public void AskPlayerName(ref string name)
        {
            name = PlayerName();
        }
    }
}
