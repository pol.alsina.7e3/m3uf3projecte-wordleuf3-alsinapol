﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worddle
{
    /// <summary>
    /// Controla la llengua utilitzada en les paraules que ha de adavinar el jugador
    /// </summary>
    public class LanguageController
    {
        public static string PathScretWords = "CatWords.txt";
        string CatWords= "CatWords.txt";
        string EngWords = "EngWords.txt";
        /// <summary>
        /// Canvia el llenguatje de les paraules, agafant un altre fitxer
        /// </summary>
        public void ChangeLanguage()
        {
            if (PathScretWords == CatWords)
            {
                PathScretWords =EngWords;
                Console.WriteLine("Idioma Canviat a Angles");
            }
            else
            {
                PathScretWords= CatWords;
                Console.WriteLine("Idioma Canviat a Catala");
            }
        }
    }
}
