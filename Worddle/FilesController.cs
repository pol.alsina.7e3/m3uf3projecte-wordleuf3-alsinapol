﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Worddle
{
    /// <summary>
    /// Clase que s'encarrega amb tot en que te a veure amb fitxer
    /// </summary>
    class FilesController
    {
        public const string PathRanking = "ranking.txt";
        List<(string, int)> ranking = new List<(string, int)>();

        /// <summary>
        /// Obre un fitxer per escollir una paraula que haura de endevinar el jugador
        /// </summary>
        /// <returns>retorna la paraula secreta</returns>
        public string GetSecretWord()
        {
            List<string> words = new List<string>(ReadFileReturnList(LanguageController.PathScretWords));
            Random random = new Random();
            int indexWordsSecret = random.Next(0, words.Count);
            return words[indexWordsSecret];         
        }
        /// <summary>
        /// obre un fitxer i retorna un llista
        /// </summary>
        /// <param name="path">fitxer a obrir</param>
        /// <returns>retorna llista string</returns>
        List<String> ReadFileReturnList(string path)
        {
            List<string> secretWords = new List<string>();
            var readFile = File.ReadAllLines(path);
            foreach (var s in readFile)
            {
                secretWords.Add(s);
            }                
            return secretWords;
        }
        /// <summary>
        /// Obre el fitxer del ranking i el mostrar
        /// </summary>
        public void ShowRanking()
        {
            Console.WriteLine("------Actaul Ranking------");
            foreach (string line in File.ReadAllLines(PathRanking))
            {
                Console.WriteLine(line);
            }
        }
        /// <summary>
        /// Metode que calcula la puntacio que ha tret el jugador. Si ho ha fet en pocs intents tindra mes puntuacio
        /// </summary>
        /// <param name="intents">Param que conte en quants intents s'ha pasat el joc</param>
        /// <returns>Retorna la puntuacio total que ha tret el jugador</returns>
        public int CalculateScore(int intents)
        {
            int score = 1300;
            for (int i = 0; i < intents; i++)
            {
                score = score - 100;
            }
            return score;
        }
        /// <summary>
        /// Metode que escriu en arxiu les cinc persones amb mes puntuacio
        /// </summary>
        /// <param name="score">Param puntuacio que ha conseguit el jugador</param>
        /// <param name="name">Param conte nom del jugador</param>
        public void WriteRanking(int score, string name)
        {
            
            ranking.Add((name,score));
            ranking = ranking.OrderByDescending(x => x.Item2).ToList();
            using (StreamWriter writer = File.CreateText(PathRanking))
            {
                int i = 0;
                foreach ((string n, int s) in ranking)
                {
                        writer.WriteLine($"{n}: {s}");
                        Console.WriteLine($"{n}: {s}");
                }
            }
            //ShowRanking();
        }

    }
}
