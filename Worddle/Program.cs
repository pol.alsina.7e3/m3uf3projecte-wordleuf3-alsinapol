﻿using System;
using System.IO;

namespace Worddle
{
    /// <summary>
    /// Clase que conte el main per inicialitzar el programa
    /// </summary>
    class Program
    {
        /// <summary>
        /// Metode main que inicialitzar el programa
        /// </summary>
        static void Main()
        {
            Directory.SetCurrentDirectory(@"E:\My Way\Worddle\Worddle");
            Menu program = new Menu();
            program.MenuController();
        }
    }
}
