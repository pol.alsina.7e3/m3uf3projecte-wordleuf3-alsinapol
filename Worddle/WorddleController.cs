﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worddle
{
    /// <summary>
    /// clase que conte diferents metodes per la bona execucio del worddle
    /// </summary>
    class WorddleController
    {
        string _secret = "";
        int _tries = 0;
        static InData data = new InData();
        static FilesController filesController = new FilesController();
        private string _name;
        /// <summary>
        /// Inicialitzar la partida del worddle
        /// </summary>
        public void WorddleStart()
        {
            _secret = filesController.GetSecretWord();
            WorddleGame();
        }
        /// <summary>
        /// diferents metodes que fan llegir la resposta del jugador i la comprova
        /// </summary>
        public void WorddleGame()
        {

            Console.WriteLine("Intents:{0}", _tries);
            //Console.WriteLine(_secret);
            string lletras = data.ReadStringAnswer();
            CheckAnswer(lletras);
            _tries++;
        }
        /// <summary>
        /// Metode que comprova si el jugador ha adivinat la paraula o nomes certes lletres o cap
        /// </summary>
        /// <param name="lletras">Param conte les lletres que ha donat l'usuari</param>
        public void CheckAnswer(string lletras)
        {
            string lletrasBe = "";
            Console.WriteLine("resultat".ToUpper());
            for (int i = 0; i < lletras.Length; i++)
            {
                if (lletras != null && lletras[i] == _secret[i])
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    lletrasBe += lletras[i];
                    Console.Write(lletras[i]);
                }
                else
                { 
                    for (int j = 0; j < lletras.Length; j++)
                    {
                        if (lletras[i] == _secret[j])
                        {
                            Console.BackgroundColor = ConsoleColor.Yellow;
                            //Console.Write(lletras[i]);
                        }
                        
                    }
                    Console.Write(lletras[i]);                 
                }
                Console.ResetColor();
            }
            Console.WriteLine();
            _tries++;
            Resultat(lletras, lletrasBe);
        }
        /// <summary>
        /// Metode que imprimeix per pantalla el resultat: si intents es 12 i no ha completat la paraula la seva puntuacio sera de 0, si l'adivinat se li mostrara la seva puntuacio i si no continua el joc 
        /// </summary>
        /// <param name="lletras">Param conte les lletres que ha donat l'usuari</param>
        /// <param name="lletrasBe">Param conte les lletres que ha donat l'usuari i estan al seu lloc</param>
        public void Resultat(string lletras, string lletrasBe)
        {
            if (_tries == 6) Console.WriteLine("No l'has endevinat （；_・）");

            if (lletras == _secret && _tries != 6)
            {
                Console.WriteLine("Has adivinat totes les lletres Felicitats. HAS GUANYAT!!!(*^^)v");
                data.AskPlayerName(ref _name);
                Ranking();
            }
            if (lletras != _secret && _tries != 6)
            {
                Console.WriteLine("Has endivinat {0} lletras i {1} estan a la seva posicio, ben fet continua!!!(^-^*)/", lletrasBe.Length, lletrasBe);
                lletrasBe = "";
                WorddleGame();
            }
        }
        /// <summary>
        /// Metode que conte diferents metodes que s'encareguen que el sistema ranking funcioni
        /// </summary>
        public void Ranking()
        {
            int score = filesController.CalculateScore(_tries);
            filesController.WriteRanking(score, _name);
            filesController.ShowRanking();
        }

    }
}
